// convert.js
const fs = require('fs');
const marked = require('marked');

// Read README.md
const markdown = fs.readFileSync('README.md', 'utf-8');

// Configure marked options
marked.setOptions({
    gfm: true, // GitHub Flavored Markdown
    breaks: true, // Convert \n to <br>
    headerIds: true, // Add ids to headers for linking
    mangle: false, // Don't escape HTML
    smartLists: true, // Use smarter list behavior
    smartypants: true // Use smart punctuation
});

// Convert markdown to HTML
const content = marked.parse(markdown);

// Create complete HTML document
const html = `<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Nick Dowdy's GitLab Page</title>
    <style>
        body { 
            font-family: system-ui, -apple-system, sans-serif; 
            line-height: 1.6; 
            max-width: 1200px; 
            margin: 40px auto; 
            padding: 0 20px; 
        }
        code { 
            background: #f4f4f4; 
            padding: 2px 5px; 
            border-radius: 3px; 
        }
        pre code { 
            display: block; 
            padding: 10px; 
            overflow-x: auto; 
        }
        img { 
            max-width: 100%; 
            height: auto; 
        }
        img[src*="nick.png"] {
            width: 150px;
            height: 150px;
            border-radius: 50%;
            float: left;
            margin-right: 20px;
        }
        /* New styles to adjust spacing */
        h4 {
            margin-bottom: 0.2em;  /* Reduce bottom margin */
            margin-top: 0.2em;     /* Reduce top margin */
        }
        h4 + h4 {
            margin-top: 0;         /* Remove top margin for consecutive h4 elements */
        }
        h4 small {
            font-weight: normal;   /* Make the text in <small> tags normal weight */
        }
    </style>
</head>
<body>
${content}
</body>
</html>`;

// Write to index.html
fs.writeFileSync('public/index.html', html);
